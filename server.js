const seneca = require('seneca')();
const crawler = require('./plugins/crawler');
const scoring = require('./plugins/scoring');
const postNotification = require('./plugins/post-notification');
const msgNotification = require('./plugins/msg-notification');

seneca.use(crawler);
seneca.use(scoring);
seneca.use(postNotification);
seneca.use(msgNotification);

seneca.listen();
