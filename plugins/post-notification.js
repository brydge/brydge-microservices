const massive = require('massive');
const generateId = require('time-uuid');

const connection = {connectionString:"postgres://brydge:bridge@localhost/brydge"};


module.exports = function notification(options) {

  this.add('service:notification,role:post,cmd:generate', function generateNotification(args, respond) {
    const post = args.post;
    if (!post) return respond({error:'Missing post param'});
    console.log("Generating notification for", post);

    massive.connect(connection, (err, db)=>{
      var data = {
        shortid:generateId(),
        title:post.title || 'New post',
        category:'post',
        ownerid:post.ownerid,
        content:post.content,
        created_at:new Date(),
        updated_at:new Date(),
        target_id:post.shortid,
        context_id:post.ownerid
      }

      db.notifications.save(data, (err, p)=>{
        if(err) return respond(err);
        respond(null, {postid:p.shortid});
      console.log("Generated notification for", p);
      });

    });

  });

}
