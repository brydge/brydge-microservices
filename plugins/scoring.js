
module.exports = function scoring(options) {

  this.add('role:scoring,cmd:score', function score(args, respond) {
    const newsfeedid = args.id;
    if (!newsfeedid) return respond({error:'Missing newsfeed id'});
    console.log(newsfeedid);
    respond(null, {newsfeed:newsfeedid});
  }); 


}
