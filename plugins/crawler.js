const cheerio = require('cheerio');
const webshot = require('webshot');
const request = require('request');
const massive = require('massive');
const hash = require("shorthash").unique;
const fs = require('fs');

const connection = {connectionString:"postgres://brydge:bridge@localhost/brydge"};

const headers = {'User-Agent':'Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.20 (KHTML, like Gecko) Mobile/7B298g'};

const webshotOptions = {
  screenSize: {
    width: 380, 
    height: 380 
  }, 
  shotSize: {
    width: 380, 
    height: 380 
  },
  userAgent:headers['User-Agent']
};

module.exports = function crawler(options) {

  this.add('role:crawler,cmd:crawl', function crawl(msg, respond) {
    const url = msg.url;

    return requestp(url).then((response, body)=>{
      const hashurl = hash(normalizeUrl(url));
      console.log("url=", url, ",", hashurl);

      massive.connect(connection, (err, db)=>{
        const cache = db.webcache.findOneSync({hashid:hashurl});
        if (cache) return respond(null, {result:cache});

        var $ = cheerio.load(response.body);
        var data = {
          title: $('meta[property="og:title"]').attr('content') || $('title').text(),
          summary: $('meta[property="og:description"]').attr('content'),
          image: $('meta[property="og:image"]').attr('content') || 'screenshot.png',
          screenshot: "screenshot.png",
          domain:"", // XXX: TODO
          url: $('meta[property="og:url"]').attr("content") || url,
          hashid:hashurl
        }

        db.webcache.save(data, (err, saved)=>{

          webshot(url, 'screenshot.png', webshotOptions, (err)=>{
            if(err) console.log("Error webshot:", err);
          });

          console.log(" >>> ", data.image, "is a", typeof data.image);

          if(!/screenshot.png/i.test(data.image))
            request(data.image).pipe(fs.createWriteStream('test.png'));

          respond(null, {url:url, data});  
        });
      });


    }).catch((err)=>{
      // console.log("Error:", err);
      respond(err);
    });
  });

}

function normalizeUrl(url) {
  var u = url.replace(/\/$/,'').toLowerCase();
  u = u.replace(/^https?:\/\//,'');
  return u;
}

function requestp(url, json) {
  json = json || false;
  return new Promise(function (resolve, reject) {
    request({url:url, json:json
      // , headers:headers
    }, function (err, res, body) {
      if (err) {
        return reject(err);
      } else if (res.statusCode !== 200) {
        err = new Error("Unexpected status code: " + res.statusCode);
        err.res = res;
        return reject(err);
      }
      console.log("Status:", res.statusCode); 
      resolve(res, body);
    });
  });
}

