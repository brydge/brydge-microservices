const massive = require('massive');
const generateId = require('time-uuid');

const connection = {connectionString:"postgres://brydge:bridge@localhost/brydge"};


module.exports = function notification(options) {

  this.add('service:notification,role:message,cmd:generate', function generateNotification(args, respond) {
    const msg = args.message;
    if (!msg) return respond({error:'Missing msg param'});
    console.log("Generating notification for", msg);

    massive.connect(connection, (err, db)=>{
      var data = {
        shortid:generateId(),
        title:msg.title || 'New message',
        category:'message',
        content:msg.content,
        created_at:new Date(),
        updated_at:new Date(),
        target_id:msg.shortid,
        ownerid:msg.to_id,
        context_id:msg.from_id
      }

      db.notifications.save(data, (err, p)=>{
        if(err) return respond(err);
        respond(null, {id:p.shortid});
        console.log("Generated notification for", msg);
      });

    });

  });

}
